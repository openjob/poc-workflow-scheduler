import airflow
from airflow import DAG
from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.operators.bash import BashOperator

# Name and schedule for the DAG
dag = DAG(
    dag_id="publisher_clicks",
    tags=["publisher_clicks"],
    start_date=airflow.utils.dates.days_ago(1),
    schedule_interval="*/15 * * * *",   # Uses cron notation. Some shortcuts are also available
    catchup=False,
)

# Three different SQL tasks and a bash command are defined below
update_clicks = PostgresOperator(
    task_id="update_clicks",
    postgres_conn_id="redshift-stable",
    sql="""
        begin;
        delete from streaming.publisher_clicks_enriched;
        insert into streaming.publisher_clicks_enriched
        select click_ts,
               min_ts,
               max_ts,
               proc_ts,
               click_id,
               file_id,
               short_id,
               job_board,
               customer_id,
               job_campaign_id,
               supercampaign_id,
               market_id,
               category,
               zipcode,
               population,
               landing_page_url,
               segment_filter_label,
               segment_filter_priority,
               flight_filter_label,
               flight_filter_priority,
               num_clicks,
               cpc,
               total_cost,
               bucket,
               key
        from (
            select *, row_number() over (partition by click_id order by proc_ts desc) as row_nr
            from streaming.publisher_clicks_enriched_staging
                 ) as clicks
        where row_nr = 1;
        commit;
    """,
    dag=dag
)

update_clicks_hr = PostgresOperator(
    task_id="update_clicks_hr",
    postgres_conn_id="redshift-stable",
    sql="""
        begin;
        delete from streaming.publisher_clicks_enriched_hr;
        insert into streaming.publisher_clicks_enriched_hr
        select
            click_ts::date as date,
            extract(hr from click_ts) as hr,
            customer_id,
            sum(num_clicks) as num_clicks,
            sum(total_cost) as total_cost
        from streaming.publisher_clicks_enriched
        group by 1,2,3;
        commit;
    """,
    dag=dag
)

update_clicks_date = PostgresOperator(
    task_id="update_clicks_date",
    postgres_conn_id="redshift-stable",
    sql="""
        begin;
        delete from streaming.publisher_clicks_enriched_date;
        insert into streaming.publisher_clicks_enriched_date
        select
            click_ts::date as date,
            customer_id,
            sum(num_clicks) as num_clicks,
            sum(total_cost) as total_cost
        from streaming.publisher_clicks_enriched
        group by 1,2;
        commit;
    """,
    dag=dag
)

echo = BashOperator(
    task_id="echo_status",
    bash_command="echo 'Update Completed'",
    start_date=airflow.utils.dates.days_ago(1),
    dag=dag
)

# Here the graph/dependencies are defined
update_clicks >> update_clicks_date
update_clicks >> update_clicks_hr
update_clicks_date >> echo
update_clicks_hr >> echo
