
# POC-workflow-scheduler
## Type of tools

Tools can be broadly classified in 3 categories:


### Traditional drag-and-drop ETL tools

**Examples**: Talend, Pentaho, etc.

They provide a drag-and-drop UI where you manage components for each test/transformation.
As such, they hide the implementation of every action.

Scripts can also be executed but if those are most of the tasks to be performed, the other categories 
are more appropriate.

Version control is often not easy or straightforward to implement.

They tend to focus more on the connectors to different platforms than the functionality.

There are free Community Edition's and tens of SaaS options.

They don't fit the way we usually work. So they don't get covered below.


### Programmatic workflows

**Examples**: Airflow, Prefect, Dagster, etc.

Workflows are declarative. Different languages for different tools but python is the most prevalent.

Most of them provide a UI where the job graph and/or the schedules/executions can be visualized.

They provide more flexibility in designing the workflow and its easier to expand functionality 
with custom plugins/operators.

Not easy to apply in-flight transformations.

They can be installed in any platform and there are multiple managed options available.

Scaling might mean adding more servers to the cluster. In that case, they don't scale per job.


### Devops style

**Examples**: Argo Workflows

Workflows look like deployments. They are declared in YAML and each task is run in its container 
in kubernetes. A graph of dependencies can be declared.

Scalability is most flexible here.


## Airflow vs Prefect ( & Dagster) vs Argo Workflows
### Declaring workflows
All these tools declare workflows in python.

Prefect and Dagster take a similar approach. Workflow steps are python functions with decorators.

Airflow uses _operators_. Operators are classes and each step in a workflow is an instance of 
a given class. There is a big ecosystem of operators for common tasks and accessing different platforms.
Common tasks we need that are already implemented:

* Executing bash commands (including executing a java jar)
* Make http calls
* Executing SQL scripts
* Executing python scripts

New operators are easy to implement.

Argo uses YAML to declare workflows. Each step declares a container and a task to run in it.


### Parallelism
Parallelism in an airflow workflow is defined by the operators dependencies.

Prefect needs to declare multiple tasks within a function and mark it for parallel execution.

Argo defines parallel tasks by nesting multiple tasks into one.

In terms of infrastructure, parallelism is limited by the number/size of executors in Airflow/Prefect.
For Argo, being based on kubernetes, it is only limited by the cluster resources.


### Async
Prefect is closer to a regular python script. So, it can implement standard async calls.

I wasn't able to find a standard "async operator" for Airflow or a straightforward way 
to run operators in an async fashion.


### Deployment
Prefect requires to use it´s CLI to register new jobs with it´s server

Airflow scans a folder/S3-location to refresh or add workflows.

For Argo, it would be a custom CI/CD pipeline.


### Architecture
They all rely on having a server, a database and, in some cases, a separate executor.

Both Airflow and Prefect can handle parallelism by setting the appropriate execution mode.

Airflow can also scale to multiple machines. Prefect seems not to have this option.

They can be deployed on any platform, kubernetes or go with a managed service.


### Managed services
There are managed options for both tools.

For airflow there are multiple options, including AWS. 
AWS's option has an easy out-of-the-box setup using S3 as a jobs repository and Cloudwatch.

No managed option for Argo


### Connections and Secrets
Airflow has a central repository of connections that can be used. Credentials can also be imported 
as variables into scripts.

No such thing in Prefect.

Argo uses kubernetes secrets/serviceAccounts.


### Scheduler
Airflow can define the running schedule in the workflow itself.

Prefect also ships with a scheduler while Argo doesnt. Argor requires use of an external 
scheduler such as ArgoCron.


### User Interface
They all provide a UI.

Airflow UI seems the most outdated but it also provide more feature to monitor jobs performance


## Example workflow

Airflow:

The below DAG has been tested with Redshift. It first updates a main table and then updates two 
other child tables concurrently. Finally, logs a status message.
It runs every 15 minutes.

The DAG generated is:

![](assets/graph.png)

And the execution:

![](assets/execution.png)

```
import airflow
from airflow import DAG
from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.operators.bash import BashOperator

# Name and schedule for the DAG
dag = DAG(
    dag_id="publisher_clicks",
    tags=["publisher_clicks"],
    start_date=airflow.utils.dates.days_ago(1),
    schedule_interval="*/15 * * * *",   # Uses cron notation. Some shortcuts are also available
    catchup=False,
)

# Three different SQL tasks and a bash command are defined below
update_clicks = PostgresOperator(
    task_id="update_clicks",
    postgres_conn_id="redshift-stable",
    sql="""
        begin;
        delete from streaming.publisher_clicks_enriched;
        insert into streaming.publisher_clicks_enriched
        select click_ts,
               min_ts,
               max_ts,
               proc_ts,
               click_id,
               file_id,
               short_id,
               job_board,
               customer_id,
               job_campaign_id,
               supercampaign_id,
               market_id,
               category,
               zipcode,
               population,
               landing_page_url,
               segment_filter_label,
               segment_filter_priority,
               flight_filter_label,
               flight_filter_priority,
               num_clicks,
               cpc,
               total_cost,
               bucket,
               key
        from (
            select *, row_number() over (partition by click_id order by proc_ts desc) as row_nr
            from streaming.publisher_clicks_enriched_staging
                 ) as clicks
        where row_nr = 1;
        commit;
    """,
    dag=dag
)

update_clicks_hr = PostgresOperator(
    task_id="update_clicks_hr",
    postgres_conn_id="redshift-stable",
    sql="""
        begin;
        delete from streaming.publisher_clicks_enriched_hr;
        insert into streaming.publisher_clicks_enriched_hr
        select
            click_ts::date as date,
            extract(hr from click_ts) as hr,
            customer_id,
            sum(num_clicks) as num_clicks,
            sum(total_cost) as total_cost
        from streaming.publisher_clicks_enriched
        group by 1,2,3;
        commit;
    """,
    dag=dag
)

update_clicks_date = PostgresOperator(
    task_id="update_clicks_date",
    postgres_conn_id="redshift-stable",
    sql="""
        begin;
        delete from streaming.publisher_clicks_enriched_date;
        insert into streaming.publisher_clicks_enriched_date
        select
            click_ts::date as date,
            customer_id,
            sum(num_clicks) as num_clicks,
            sum(total_cost) as total_cost
        from streaming.publisher_clicks_enriched
        group by 1,2;
        commit;
    """,
    dag=dag
)

echo = BashOperator(
    task_id="echo_status",
    bash_command="echo 'Update Completed'",
    start_date=airflow.utils.dates.days_ago(1),
    dag=dag
)

# Here the graph/dependencies are defined
update_clicks >> update_clicks_date
update_clicks >> update_clicks_hr
update_clicks_date >> echo
update_clicks_hr >> echo
```


## Running it yourself

The example above was tested on Redshift STABLE.

If you want to run it yourself, you can use the `docker-compose.yaml` 
in the `airflow` folder.

Access the UI at `http://localhost:8080` (user/password: `airflow`)

You will need to create a database connection in the Admin console like 
the following:

![](assets/connection.png)
